import * as React from 'react';
import Head from 'next/head';

export default class Home extends React.Component {
  render () {
    return (
      <main className="page">
        <Head>
          <title>Share.it</title>
        </Head>
        <div>
          <h1>Helloworld!</h1>
        </div>
      </main>
    );
  }
}
