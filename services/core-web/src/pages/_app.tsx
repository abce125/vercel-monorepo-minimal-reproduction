import * as React from 'react';
import { withRouter } from 'next/router';
import { sayHello } from '@vercel-monorepo-minimal-reproduction/common';

import type { AppProps } from 'next/app';

import '../styles/globals.css';

class App extends React.Component<AppProps> {
  render () {
    sayHello();
    const { Component, pageProps } = this.props;
    return (
      <Component {...pageProps} />
    );
  }
}

export default withRouter(App);
