import type { Request, Response } from 'express';

import express from 'express';
import { sayHello } from '@vercel-monorepo-minimal-reproduction/common';
import { sayGoodbye } from './goodbye';

const app = (() => {
  const app = express();

  app.get('/', (req: Request, res: Response) => {
    sayHello();
    sayGoodbye();
    res.status(200);
    res.json({ statusCode: 200, message: 'Ok' });
  });

  app.use((req: Request, res: Response) => {
    sayHello();
    sayGoodbye();
    res.status(404);
    res.json({ statusCode: 404, message: 'Not Found' });
  });

  app.listen(process.env.PORT || 3000, () => {
    console.log('app is running on port: 3000');
  });

  return app;
})();

export default app;
