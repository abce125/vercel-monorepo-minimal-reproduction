# vercel-monorepo-minimal-reproduction

### Version
|    Tool    |  Version  |
| ---------- | --------- |
| yarn       | = 1.22.10 |
| node       | = 14.17.0 |
| vercel-cli | = 23.0.1  |

### Requirements to use Vercel-cli in CI pipelines
  - From gitlab-ci you must set `VERCEL_TOKEN`, `VERCEL_ORG_ID`, `VERCEL_PROJECT_ID_CORE_API` and `VERCEL_PROJECT_ID_CORE_WEB` environment variables from `Settings > CI/CD > Variables`.
  - From gitlab-ci protect scoped tags `core-api/*` and `core-web/*` from `Settings > Repository > Protected Tags`.
  - Push a scoped tag from anywhere.
